from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta

import os 
os.chdir(os.path.dirname(__file__))

from airflow.utils.dates import days_ago
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from time import sleep
from utils.slack_notification import SlackNotification

import numpy as np
from numpy.lib.shape_base import column_stack
import pandas as pd
from dotenv import load_dotenv, main
from pandas.core import groupby
from sqlalchemy import create_engine

load_dotenv(dotenv_path='utils/.env')

class Test():
    def __init__(self, startdate = None, enddate = None):
        self.sqlEngineGB = create_engine(f"mysql+pymysql://{os.getenv('mysql_user')}:{os.getenv('mysql_pw')}@{os.getenv('mysql_host')}/geniebook",pool_recycle=3600)
        self.sqlEngineTest = create_engine(f"mysql+pymysql://{os.getenv('mysql_user')}:{os.getenv('mysql_pw')}@{os.getenv('mysql_host')}/Test",pool_recycle=3600)
        os.makedirs('data', exist_ok=True)
        
        self.startdate = (datetime.now() - timedelta(days=30)).strftime('%Y-%m-%d')
        self.enddate = (datetime.now() - timedelta(days=29)).strftime('%Y-%m-%d')
        self.swa = self.read_student_worksheet_answer(self.startdate, self.enddate)

    def get_student_worksheet_answer(self, startdate = None, enddate = None):
        if startdate is None:
            startdate = '20211011'
            enddate   = '20211012'
        #Query Student Worksheet Answer from GB db
        query = '''
                SELECT *
                FROM geniebook.student_worksheet_answer 
                WHERE date_created >= %s AND date_created < %s;
                '''
        
        swa = pd.read_sql_query(query, self.sqlEngineGB, params=[startdate, enddate])

        swa.to_pickle('data/t_k_'+startdate+'_student_worksheet_answer.pkl')

    def read_student_worksheet_answer(self, startdate = None, enddate = None):
        if 't_k_'+startdate+'_student_worksheet_answer.pkl' not in os.listdir('data/'):
            last_date = (datetime.strptime(startdate, '%Y-%m-%d') - timedelta(days=1)).strftime('%Y-%m-%d')
            for f in os.listdir('data/'):
                if 't_k_'+last_date+'_student_worksheet_answer.pkl' in f:
                    os.remove(f'data/{f}')
            self.get_student_worksheet_answer(startdate, enddate)
        if 't_k_'+startdate+'_student_worksheet_answer.pkl' in os.listdir('data/'):
            return pd.read_pickle('data/t_k_'+startdate+'_student_worksheet_answer.pkl') 
        else:
            raise Exception('unable to read t_k_'+startdate+'_student_worksheet_answer')

    def simulate_swa_write(self):
        now = datetime.now()
        before = now - timedelta(minutes=5)
        now_f = now.strftime('%H:%M:00')
        before_f = before.strftime('%H:%M:00')
        now = datetime.strptime(f'{self.startdate} {now_f}', '%Y-%m-%d %H:%M:%S')
        before = datetime.strptime(f'{self.startdate} {before_f}', '%Y-%m-%d %H:%M:%S')

        swa = self.swa.loc[lambda x: x.date_created < now]\
                      .loc[lambda x: x.date_created >= before]

        return swa

    def upload_sql(self, table='TEST_KAFKA_student_worksheet_answer'):
        data = self.simulate_swa_write()
        data.to_sql(table, self.sqlEngineTest, if_exists='append', index=False, dtype=None, index_label='index', chunksize=None, method=None)


default_args = {
	'owner': 'dio',
	'start_date': days_ago(1),
	'on_failure_callback': SlackNotification.failure_notification,
    'retries': 0,
}

sim = Test()

with DAG('kafka_test_sim_swa',
	default_args=default_args,
	schedule_interval='*/5 * * * *',
	sla_miss_callback=SlackNotification.sla_notification,
	catchup=False) as dag:
    
    t1 = DummyOperator(
		task_id='start'
	)
    
    t2 = PythonOperator(
        task_id='simulate',
        python_callable=sim.upload_sql,
    )
    
    t3 = DummyOperator(
		task_id='end'
	)
    
t1 >> t2 >> t3

# if __name__ == "__main__":
#     sim = Test()
#     sim.upload_sql()