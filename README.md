# README #

This is a Docker Compose and Kafka Test Files. You need to install docker first before start.

### The Docker Compose Will Build an Image and Install ###

* Zookeeper
* Kafka
* Kafka Connector (Debezium v1.7, MySQL J Connector v8.0.27, JDBC Kafka Connector v10.2.2)

### How do I get set up? ###

#### Build the Image ####

Grab Dockerfile and `docker-compose.yaml`. Then, You need to build Kafka Connector image first, Run :
```
docker-compose build
```

#### Create and Run Container ####

To Create and Run the container using docker-compose, Run:
```
docker-compose up -d
```

#### Setup Database ####

Before to start a debezium connector please make sure already run `binlog` file on `mysql`.
To Configure `binlog` file on `mysql` you can crate or edit `mysqld` configuration on `/etc/mysql/conf.d/my.cnf` and add :
```
[mysqld]
log-bin			= mysql-bin
expire_logs_days	= 7
binlog_format		= row

```

Create new SQL User or add current user with one or more of this permission :

`SUPER` or `REPLICATION CLIENT` privileges



### Kafka Connector API ###

* View Connector Available
```
curl -i -X GET <ip-kafka-instance>:8083/connectors
```
* Start New Connector
```
curl -i -X POST -H "Accept:application/json" -H "Content-Type:application/json" <ip-kafka-instance>:8083/connectors/ -d @<your-connector-config-name.json>
```
* Delete Connector
```
curl -X DELETE <ip-kafka-instance>:8083/connectors/<connectors>
```

#### You can contact me on Slack if there's any problem or need to update  ####